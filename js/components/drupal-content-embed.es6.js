/* eslint func-names: ["error", "never"] */
(function(wp, $, Drupal, DrupalGutenberg, drupalSettings) {
  const { element, components, blockEditor } = wp;
  const { Component, Fragment } = element;
  const { Placeholder, PanelBody, SelectControl, RangeControl, ToolbarGroup, ToolbarButton } = components;
  const { BlockControls, InspectorControls, BlockIcon} = blockEditor;
  const __ = Drupal.t;

  async function getContent(item, viewMode = 'default') {
    const response = await fetch(
      Drupal.url(`editor/content/load/${item}/${viewMode}`),
      {
        method: 'GET',
        headers: {
          'Accept': 'application/json'
        }
      }
    );
    const content = await response.json();

    return content;
  }

  function processHtml(html) {
    const node = document.createElement('div');
    node.innerHTML = html;

    // Disable form elements.
    const formElements = node.querySelectorAll(
      'input, select, button, textarea, checkbox, radio',
    );
    formElements.forEach(element => {
      element.setAttribute('readonly', true);
      element.setAttribute('required', false);
      element.setAttribute('disabled', true);
    });

    // Disable links.
    const linkElements = node.querySelectorAll('a');
    linkElements.forEach(element => {
      element.setAttribute('href', 'javascript:void(0)');
    });

    return node.innerHTML;
  }

  function hasEmptyContent(html) {
    const node = document.createElement('div');
    node.innerHTML = html;

    return node.innerText.trim() ? false : true;
  }

  class DrupalContentEmbed extends Component {
    constructor() {
      super(...arguments);
      this.state = {
        html: '',
        activeSuggestion: 0,
        filteredSuggestions: [],
        showSuggestions: false,
        userInput: ""
      };
      // Loading html if we have the node id.
      const { nodeId, viewMode } = this.props.attributes;
      if (nodeId !== undefined) {
        getContent(nodeId, viewMode)
          .then(content => {
            this.setState({
              html: content
            });
          })
      }
      // Binding this.
      this.onKeyDown = this.onKeyDown.bind(this);
      this.onChange = this.onChange.bind(this);
      this.onClick = this.onClick.bind(this);
      this.clearContent = this.clearContent.bind(this);
    }

    componentDidMount() {
      const { nodeId } = this.props.attributes;
      if (nodeId) {
        // Dispatching event.
        const event = new CustomEvent('contentEmbedded', { detail: { contentType: this.props.contentType } });
        document.dispatchEvent(event);
      }
    }

    onChange(event) {
      const { contentType } = this.props;
      const userInput = event.currentTarget.value;
      if (userInput) {
        fetch('/editor/search-content/' + contentType + '/' + userInput, {
          method: 'GET',
          headers: {
            'Accept': 'application/json'
          }
        }).then((response) => {
          if (response.ok) {
            response.json().then(data => {
              this.setState({
                activeSuggestion: 0,
                filteredSuggestions: data,
                showSuggestions: true,
                userInput: userInput
              });
            })
          }
          else {
            // Show in console on error.
            console.log(response);
          }
        });
      }
      else {
        this.setState({
          activeSuggestion: 0,
          filteredSuggestions: [],
          showSuggestions: false,
          userInput: userInput
        });
      }
    }

    onClick(event) {
      const nid = event.currentTarget.getAttribute('nid');
      if (nid) {
        const nodeLabel = event.currentTarget.innerText;
        const { viewModes } = this.props;
        const initialViewMode = Object.keys(viewModes)[0];
        this.props.setAttributes({
          nodeId: parseInt(nid),
          viewMode: initialViewMode
        });

        getContent(nid, initialViewMode)
          .then(content => {
            this.setState({
              html: content,
              userInput: nodeLabel
            });
            // Dispatching event.
            const event = new CustomEvent('contentEmbedded', { detail: { contentType: this.props.contentType } });
            document.dispatchEvent(event);
          })
          .catch(r => {
            this.setState({
              html: __('An error occured when loading the content.') + r,
              userInput: nodeLabel
            });
          });
      }
    }

    onKeyDown(event) {
      const { activeSuggestion, filteredSuggestions } = this.state;
      if (event.keyCode === 13) {
        this.setState({
          activeSuggestion: 0,
          showSuggestions: false,
          userInput: filteredSuggestions[activeSuggestion]
        });
      } else if (event.keyCode === 38) {
        if (activeSuggestion === 0) {
          return;
        }
        this.setState({ activeSuggestion: activeSuggestion - 1 });
      }
      // User pressed the down arrow, increment the index
      else if (event.keyCode === 40) {
        if (activeSuggestion - 1 === filteredSuggestions.length) {
          return;
        }
        this.setState({ activeSuggestion: activeSuggestion + 1 });
      }
    }

    clearContent(event) {
      this.props.setAttributes({
        nodeId: null,
        viewMode: null
      });
      this.setState({
        html: '',
      });
    }

    render() {
      const {
        onChange,
        onClick,
        onKeyDown,
        clearContent,
        state: {
          html,
          activeSuggestion,
          filteredSuggestions,
          showSuggestions,
          userInput
        }
      } = this;
      const { className, viewModes, attributes, setAttributes, contentTypeLabel, widthControl } = this.props;
      // Suggestions List.
      let suggestionsListComponent;
      if (showSuggestions && userInput) {
        if (filteredSuggestions.length) {
          suggestionsListComponent = (
            <ul class="suggestions">
              {filteredSuggestions.map((suggestion, index) => {
                let className;

                // Flag the active suggestion with a class
                if (index === activeSuggestion) {
                  className = "suggestion-active";
                }
                return (
                  <li className={className} key={suggestion.id} nid={suggestion.id} onClick={onClick}>
                    {suggestion.title}
                  </li>
                );
              })}
            </ul>
          );
        } else {
          suggestionsListComponent = (
            <div class="no-suggestions">
              <em>No suggestions available.</em>
            </div>
          );
        }
      }
      // View modes options.
      let viewModesOptions = []
      for (const viewModeId in viewModes) {
        if (Object.hasOwnProperty.call(viewModes, viewModeId)) {
          const viewModeLabel = viewModes[viewModeId];
          viewModesOptions.push({ value: viewModeId, label: viewModeLabel });
        }
      }
      // Add width to class name.
      let componentClass = className;
      if (widthControl && widthControl.includes(attributes.viewMode)) {
        componentClass += ' width-' + attributes.pageWidth;
      }
      // Render.
      return (
        <Fragment>
          { html ?
              <Fragment>
                <div
                  className={componentClass}
                  // eslint-disable-next-line react/no-danger
                  dangerouslySetInnerHTML={{ __html: processHtml(html) }}
                />
                {hasEmptyContent(html) && (
                  <Placeholder
                    label={ userInput }
                    instructions={ __('This content is rendering empty content.') }
                  >
                  </Placeholder>
                )}
                <InspectorControls>
                    <Fragment>
                      <PanelBody title={__('Content Settings')} initialOpen={false}>
                        <SelectControl
                          label={__('View mode')}
                          value={attributes.viewMode}
                          options={viewModesOptions}
                          onChange={newViewMode => {
                            setAttributes({
                              viewMode: newViewMode,
                              pageWidth: 100
                            });
                            getContent(attributes.nodeId, newViewMode)
                              .then(content => {
                                this.setState({
                                  html: content,
                                });
                              })
                              .catch(r => {
                                this.setState({
                                  html: __('An error occured when loading the content.') + r,
                                });
                              });

                            // Dispatching event.
                            const event = new CustomEvent('changedViewMode', { detail: { contentType: this.props.contentType, viewMode: newViewMode } });
                            document.dispatchEvent(event);
                          }}
                        />
                      </PanelBody>
                      { (widthControl && widthControl.includes(attributes.viewMode)) &&
                        <PanelBody title={ __('Content Width Percentage') }>
                          <RangeControl
                            value={ attributes.pageWidth }
                            onChange={ ( width ) => setAttributes({
                              pageWidth: width
                            }) }
                            min={ 25 }
                            max={ 100 }
                            step={ 25 }
                            help={ __('Content width percentage relative to the total page width on Desktop/Large devices.') }
                          />
                        </PanelBody>
                      }
                    </Fragment>
                </InspectorControls>
                <BlockControls>
                  <ToolbarGroup>
                    <ToolbarButton
                      icon="no"
                      label={__('Clear content')}
                      onClick={clearContent}
                    />
                  </ToolbarGroup>
                </BlockControls>
              </Fragment>
            :
              <Fragment>
                <Placeholder
                  icon={<BlockIcon icon="media-document" />}
                  label={__('Content :') + contentTypeLabel}
                  instructions={__('Start typing to find the content you want to embed.')}
                  className="content-embed-autocomplete"
                >
                  <input
                    type="text"
                    onChange={onChange}
                    onKeyDown={onKeyDown}
                    value={userInput}
                    placeholder={__('Search for content...')}
                  />
                  {suggestionsListComponent}
                </Placeholder>
              </Fragment>
          }
        </Fragment>
      );
    }
  }

  window.DrupalGutenberg = window.DrupalGutenberg || {};
  window.DrupalGutenberg.Components = window.DrupalGutenberg.Components || {};
  window.DrupalGutenberg.Components.DrupalContentEmbed = DrupalContentEmbed;
})(wp, jQuery, Drupal, DrupalGutenberg, drupalSettings);
