/**
* DO NOT EDIT THIS FILE.
* See the following change record for more information,
* https://www.drupal.org/node/2815083
* @preserve
**/
"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (obj) { return typeof obj; } : function (obj) { return obj && "function" == typeof Symbol && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }, _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); Object.defineProperty(subClass, "prototype", { writable: false }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } else if (call !== void 0) { throw new TypeError("Derived constructors may only return object or undefined"); } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

(function (wp, $, Drupal, DrupalGutenberg, drupalSettings) {
  var element = wp.element,
      components = wp.components,
      blockEditor = wp.blockEditor;
  var Component = element.Component,
      Fragment = element.Fragment;
  var Placeholder = components.Placeholder,
      PanelBody = components.PanelBody,
      SelectControl = components.SelectControl,
      RangeControl = components.RangeControl,
      ToolbarGroup = components.ToolbarGroup,
      ToolbarButton = components.ToolbarButton;
  var BlockControls = blockEditor.BlockControls,
      InspectorControls = blockEditor.InspectorControls,
      BlockIcon = blockEditor.BlockIcon;
  var __ = Drupal.t;

  function getContent(_x) {
    return _getContent.apply(this, arguments);
  }

  function _getContent() {
    _getContent = _asyncToGenerator(regeneratorRuntime.mark(function _callee(item) {
      var viewMode,
          response,
          content,
          _args = arguments;
      return regeneratorRuntime.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              viewMode = _args.length > 1 && _args[1] !== undefined ? _args[1] : 'default';
              _context.next = 3;
              return fetch(Drupal.url("editor/content/load/".concat(item, "/").concat(viewMode)), {
                method: 'GET',
                headers: {
                  'Accept': 'application/json'
                }
              });

            case 3:
              response = _context.sent;
              _context.next = 6;
              return response.json();

            case 6:
              content = _context.sent;
              return _context.abrupt("return", content);

            case 8:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    }));
    return _getContent.apply(this, arguments);
  }

  function processHtml(html) {
    var node = document.createElement('div');
    node.innerHTML = html;
    var formElements = node.querySelectorAll('input, select, button, textarea, checkbox, radio');
    formElements.forEach(function (element) {
      element.setAttribute('readonly', true);
      element.setAttribute('required', false);
      element.setAttribute('disabled', true);
    });
    var linkElements = node.querySelectorAll('a');
    linkElements.forEach(function (element) {
      element.setAttribute('href', 'javascript:void(0)');
    });
    return node.innerHTML;
  }

  function hasEmptyContent(html) {
    var node = document.createElement('div');
    node.innerHTML = html;
    return node.innerText.trim() ? false : true;
  }

  var DrupalContentEmbed = function (_Component) {
    _inherits(DrupalContentEmbed, _Component);

    var _super = _createSuper(DrupalContentEmbed);

    function DrupalContentEmbed() {
      var _this;

      _classCallCheck(this, DrupalContentEmbed);

      _this = _super.apply(this, arguments);
      _this.state = {
        html: '',
        activeSuggestion: 0,
        filteredSuggestions: [],
        showSuggestions: false,
        userInput: ""
      };
      var _this$props$attribute = _this.props.attributes,
          nodeId = _this$props$attribute.nodeId,
          viewMode = _this$props$attribute.viewMode;

      if (nodeId !== undefined) {
        getContent(nodeId, viewMode).then(function (content) {
          _this.setState({
            html: content
          });
        });
      }

      _this.onKeyDown = _this.onKeyDown.bind(_assertThisInitialized(_this));
      _this.onChange = _this.onChange.bind(_assertThisInitialized(_this));
      _this.onClick = _this.onClick.bind(_assertThisInitialized(_this));
      _this.clearContent = _this.clearContent.bind(_assertThisInitialized(_this));
      return _this;
    }

    _createClass(DrupalContentEmbed, [{
      key: "componentDidMount",
      value: function componentDidMount() {
        var nodeId = this.props.attributes.nodeId;

        if (nodeId) {
          var event = new CustomEvent('contentEmbedded', {
            detail: {
              contentType: this.props.contentType
            }
          });
          document.dispatchEvent(event);
        }
      }
    }, {
      key: "onChange",
      value: function onChange(event) {
        var _this2 = this;

        var contentType = this.props.contentType;
        var userInput = event.currentTarget.value;

        if (userInput) {
          fetch('/editor/search-content/' + contentType + '/' + userInput, {
            method: 'GET',
            headers: {
              'Accept': 'application/json'
            }
          }).then(function (response) {
            if (response.ok) {
              response.json().then(function (data) {
                _this2.setState({
                  activeSuggestion: 0,
                  filteredSuggestions: data,
                  showSuggestions: true,
                  userInput: userInput
                });
              });
            } else {
              console.log(response);
            }
          });
        } else {
          this.setState({
            activeSuggestion: 0,
            filteredSuggestions: [],
            showSuggestions: false,
            userInput: userInput
          });
        }
      }
    }, {
      key: "onClick",
      value: function onClick(event) {
        var _this3 = this;

        var nid = event.currentTarget.getAttribute('nid');

        if (nid) {
          var nodeLabel = event.currentTarget.innerText;
          var viewModes = this.props.viewModes;
          var initialViewMode = Object.keys(viewModes)[0];
          this.props.setAttributes({
            nodeId: parseInt(nid),
            viewMode: initialViewMode
          });
          getContent(nid, initialViewMode).then(function (content) {
            _this3.setState({
              html: content,
              userInput: nodeLabel
            });

            var event = new CustomEvent('contentEmbedded', {
              detail: {
                contentType: _this3.props.contentType
              }
            });
            document.dispatchEvent(event);
          }).catch(function (r) {
            _this3.setState({
              html: __('An error occured when loading the content.') + r,
              userInput: nodeLabel
            });
          });
        }
      }
    }, {
      key: "onKeyDown",
      value: function onKeyDown(event) {
        var _this$state = this.state,
            activeSuggestion = _this$state.activeSuggestion,
            filteredSuggestions = _this$state.filteredSuggestions;

        if (event.keyCode === 13) {
          this.setState({
            activeSuggestion: 0,
            showSuggestions: false,
            userInput: filteredSuggestions[activeSuggestion]
          });
        } else if (event.keyCode === 38) {
          if (activeSuggestion === 0) {
            return;
          }

          this.setState({
            activeSuggestion: activeSuggestion - 1
          });
        } else if (event.keyCode === 40) {
          if (activeSuggestion - 1 === filteredSuggestions.length) {
            return;
          }

          this.setState({
            activeSuggestion: activeSuggestion + 1
          });
        }
      }
    }, {
      key: "clearContent",
      value: function clearContent(event) {
        this.props.setAttributes({
          nodeId: null,
          viewMode: null
        });
        this.setState({
          html: ''
        });
      }
    }, {
      key: "render",
      value: function render() {
        var _this4 = this;

        var onChange = this.onChange,
            onClick = this.onClick,
            onKeyDown = this.onKeyDown,
            clearContent = this.clearContent,
            _this$state2 = this.state,
            html = _this$state2.html,
            activeSuggestion = _this$state2.activeSuggestion,
            filteredSuggestions = _this$state2.filteredSuggestions,
            showSuggestions = _this$state2.showSuggestions,
            userInput = _this$state2.userInput;
        var _this$props = this.props,
            className = _this$props.className,
            viewModes = _this$props.viewModes,
            attributes = _this$props.attributes,
            setAttributes = _this$props.setAttributes,
            contentTypeLabel = _this$props.contentTypeLabel,
            widthControl = _this$props.widthControl;
        var suggestionsListComponent;

        if (showSuggestions && userInput) {
          if (filteredSuggestions.length) {
            suggestionsListComponent = React.createElement("ul", {
              class: "suggestions"
            }, filteredSuggestions.map(function (suggestion, index) {
              var className;

              if (index === activeSuggestion) {
                className = "suggestion-active";
              }

              return React.createElement("li", {
                className: className,
                key: suggestion.id,
                nid: suggestion.id,
                onClick: onClick
              }, suggestion.title);
            }));
          } else {
            suggestionsListComponent = React.createElement("div", {
              class: "no-suggestions"
            }, React.createElement("em", null, "No suggestions available."));
          }
        }

        var viewModesOptions = [];

        for (var viewModeId in viewModes) {
          if (Object.hasOwnProperty.call(viewModes, viewModeId)) {
            var viewModeLabel = viewModes[viewModeId];
            viewModesOptions.push({
              value: viewModeId,
              label: viewModeLabel
            });
          }
        }

        var componentClass = className;

        if (widthControl && widthControl.includes(attributes.viewMode)) {
          componentClass += ' width-' + attributes.pageWidth;
        }

        return React.createElement(Fragment, null, html ? React.createElement(Fragment, null, React.createElement("div", {
          className: componentClass,
          dangerouslySetInnerHTML: {
            __html: processHtml(html)
          }
        }), hasEmptyContent(html) && React.createElement(Placeholder, {
          label: userInput,
          instructions: __('This content is rendering empty content.')
        }), React.createElement(InspectorControls, null, React.createElement(Fragment, null, React.createElement(PanelBody, {
          title: __('Content Settings'),
          initialOpen: false
        }, React.createElement(SelectControl, {
          label: __('View mode'),
          value: attributes.viewMode,
          options: viewModesOptions,
          onChange: function onChange(newViewMode) {
            setAttributes({
              viewMode: newViewMode,
              pageWidth: 100
            });
            getContent(attributes.nodeId, newViewMode).then(function (content) {
              _this4.setState({
                html: content
              });
            }).catch(function (r) {
              _this4.setState({
                html: __('An error occured when loading the content.') + r
              });
            });
            var event = new CustomEvent('changedViewMode', {
              detail: {
                contentType: _this4.props.contentType,
                viewMode: newViewMode
              }
            });
            document.dispatchEvent(event);
          }
        })), widthControl && widthControl.includes(attributes.viewMode) && React.createElement(PanelBody, {
          title: __('Content Width Percentage')
        }, React.createElement(RangeControl, {
          value: attributes.pageWidth,
          onChange: function onChange(width) {
            return setAttributes({
              pageWidth: width
            });
          },
          min: 25,
          max: 100,
          step: 25,
          help: __('Content width percentage relative to the total page width on Desktop/Large devices.')
        })))), React.createElement(BlockControls, null, React.createElement(ToolbarGroup, null, React.createElement(ToolbarButton, {
          icon: "no",
          label: __('Clear content'),
          onClick: clearContent
        })))) : React.createElement(Fragment, null, React.createElement(Placeholder, {
          icon: React.createElement(BlockIcon, {
            icon: "media-document"
          }),
          label: __('Content :') + contentTypeLabel,
          instructions: __('Start typing to find the content you want to embed.'),
          className: "content-embed-autocomplete"
        }, React.createElement("input", {
          type: "text",
          onChange: onChange,
          onKeyDown: onKeyDown,
          value: userInput,
          placeholder: __('Search for content...')
        }), suggestionsListComponent)));
      }
    }]);

    return DrupalContentEmbed;
  }(Component);

  window.DrupalGutenberg = window.DrupalGutenberg || {};
  window.DrupalGutenberg.Components = window.DrupalGutenberg.Components || {};
  window.DrupalGutenberg.Components.DrupalContentEmbed = DrupalContentEmbed;
})(wp, jQuery, Drupal, DrupalGutenberg, drupalSettings);