<?php

namespace Drupal\gutenberg_content_embed\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Drupal\node\Entity\Node;

/**
 * Controller for handling node entity queries to use as URLs.
 */
class ContentController extends ControllerBase {

  /**
   * Return a list of nodes containing a piece of search text.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   * @param string $type
   *   The content type.
   * @param string $search
   *   The search string.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The JSON response.
   *
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function search(Request $request, string $type = '', string $search = '') {
    $limit = (int) $request->query->get('per_page', 20);

    $query = \Drupal::entityQuery('node');
    $query->condition('title', $search, 'CONTAINS')
      ->condition('type', $type)
      ->condition('status', 1)
      ->sort('created', 'DESC')
      ->range(0, $limit)
      ->accessCheck(TRUE);

    $node_ids = $query->execute();
    $nodes = Node::loadMultiple($node_ids);
    $result = [];
    foreach ($nodes as $node) {
      $result[] = [
        'id' => $node->id(),
        'title' => $node->getTitle(),
      ];
    }

    return new JsonResponse($result);
  }

  /**
   * Returns JSON representing the loaded node.
   *
   * @param int $nid
   *   Node ID.
   * @param int $viewmode
   *   The view mode to render.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The JSON response.
   */
  public function loadById($nid, $viewmode = 'default') {
    $view_builder = \Drupal::entityTypeManager()->getViewBuilder('node');
    $node = \Drupal::entityTypeManager()->getStorage('node')->load($nid);
    if ($node) {
      if ($node->access('view')) {
        $build = $view_builder->view($node, $viewmode);
        return new JsonResponse(\Drupal::service('renderer')->render($build));
      }
      else {
        return new JsonResponse($this->t('You cannot view this content.'));
      }
    }
    return new JsonResponse($this->t('Unable to render node'));
  }

}
