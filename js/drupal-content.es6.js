/* eslint func-names: ["error", "never"] */
(function(wp, Drupal, drupalSettings) {
  const { data, blocks, blockEditor } = wp;
  const { Fragment } = wp.element;
  const { DrupalContentEmbed } = window.DrupalGutenberg.Components;

  function registerBlock(id, label, viewmodes, widthControl) {
    const blockId = `drupalcontent/${id.replace(/_/g, "-")}`;
    blocks.registerBlockType(blockId, {
      title: Drupal.t('Content') + ': ' + label,
      icon: 'media-document',
      category: 'drupalcontent',
      supports: {
        align: true,
        html: false,
        reusable: false,
      },
      attributes: {
        nodeId: {
          type: 'number',
        },
        viewMode: {
          type: 'string',
          default: 'default',
        },
        pageWidth: {
          type: "number",
          default: 100
        },
      },
      edit({ attributes, className, setAttributes }) {
        return (
          <DrupalContentEmbed
            attributes={attributes}
            className={className + ' gutenberg-content-embed'}
            contentType={id}
            contentTypeLabel={label}
            setAttributes={setAttributes}
            viewModes={viewmodes}
            widthControl={widthControl}
          />
        );
      },
      save() {
        return null;
      },
    });
  }

  function registerDrupalContent() {
    const category = {
      slug: 'drupalcontent',
      title: Drupal.t('Drupal Content'),
    };

    const categories = [
      ...data.select('core/blocks').getCategories(),
      category,
    ];

    data.dispatch('core/blocks').setCategories(categories);
    if (drupalSettings.gutenbergContentEmbed.allowed !== undefined) {
      const allowedContentTypes = drupalSettings.gutenbergContentEmbed.allowed;
      for (const key in allowedContentTypes) {
        if (Object.hasOwnProperty.call(allowedContentTypes, key)) {
          const viewmodes = allowedContentTypes[key]['viewModes'];
          const widthControl = allowedContentTypes[key]['widthControl'];
          const label = allowedContentTypes[key]['label'];
          registerBlock(key, label, viewmodes, widthControl)
        }
      }
    }
  }
  registerDrupalContent();
})(wp, Drupal, drupalSettings);
