<?php

namespace Drupal\gutenberg_content_embed\BlockProcessor;

use Drupal\Component\Utility\Html;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Cache\RefinableCacheableDependencyInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\gutenberg\BlockProcessor\GutenbergBlockProcessorInterface;
use Psr\Log\LoggerInterface;

/**
 * Processes Gutenberg embeded content.
 */
class DrupalContentProcessor implements GutenbergBlockProcessorInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Drupal\Core\Render\RendererInterface instance.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * The Gutenberg logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * ReusableBlockProcessor constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer.
   * @param \Psr\Log\LoggerInterface $logger
   *   Gutenberg logger interface.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    RendererInterface $renderer,
    LoggerInterface $logger
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->renderer = $renderer;
    $this->logger = $logger;
  }

  /**
   * {@inheritdoc}
   */
  public function processBlock(array &$block, &$block_content, RefinableCacheableDependencyInterface $bubbleable_metadata) {
    $block_attributes = $block['attrs'];
    if (empty($block_attributes['nodeId'])) {
      $this->logger->error('Missing content ID.');
      return;
    }

    $nid = $block_attributes['nodeId'];
    $viewmode = $block_attributes['viewMode'] ?? 'default';
    $node = $this
      ->entityTypeManager
      ->getStorage('node')
      ->load($nid);

    if ($node && $node->access('view')) {
      $render_content = $this
        ->entityTypeManager
        ->getViewBuilder('node')
        ->view($node, $viewmode);

      $render = [
        '#type' => 'container',
        '#attributes' => [
          'class' => [
            'content-embed',
            Html::getClass($node->bundle() . '-' . $viewmode),
          ],
        ],
        'content' => $render_content,
      ];

      // Add align class if available.
      if (!empty($block_attributes['align'])) {
        $render['#attributes']['class'][] = Html::getClass('align-' . $block_attributes['align']);
      }
      // Add the css class if available.
      if (!empty($block_attributes['className'])) {
        $render['#attributes']['class'][] = Html::escape($block_attributes['className']);
      }
      // Add width class if available.
      if (!empty($block_attributes['pageWidth'])) {
        $render['#attributes']['class'][] = Html::getClass('width-' . $block_attributes['pageWidth']);
      }

      $block_content = $this->renderer->render($render);

      $bubbleable_metadata->addCacheableDependency(
        CacheableMetadata::createFromRenderArray($render)
      );
    }
  }

  /**
   * {@inheritdoc}
   */
  public function isSupported(array $block, $block_content = '') {
    return substr($block['blockName'] ?? '', 0, 14) === 'drupalcontent/';
  }

}
