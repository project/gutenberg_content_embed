# Gutenberg Content Embed

This module provides new Gutenberg blocks to embed content on pages,
admin users can control which content types and view modes are available for the editors.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/gutenberg_content_embed).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/gutenberg_content_embed).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module requires the following modules:

- [Gutenberg](https://www.drupal.org/project/gutenberg)


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

- The admin users can control the available content types and view modes per content type on:
  `_admin/structure/types/manage/[CONTENT_TYPE]_`.
- Once in the content type edit form go to Gutenberg experience tab and adjust settings in the
  ALLOWED CONTENT fieldset.


## Maintainers

- Ivan Duarte - [jidrone](https://www.drupal.org/u/jidrone).
